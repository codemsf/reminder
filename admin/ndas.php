<?php session_start(); include '../koneksi.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
   <link rel="stylesheet" type="text/css" href="../data/css/bootstrap.css">
  <script type="text/javascript" src="../data/js/jquery.js"></script>
  <script type="text/javascript" src="../data/js/bootstrap.js"></script>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand">SUPERADMIN PANEL</a>
      <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown">MENU<b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="index.php">BERANDA</a></li>
            <li><a href="daftaradmin.php">MANAJEMEN ADMIN</a></li>
            <li><a href="daftaruser.php">MANAJEMEN USER</a></li>
          </ul>
        </li>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right navbar-brand">
           <li ><p>Halo Admin <b><?php echo $_SESSION['status']; ?></b>
          <a class="btn btn-danger" href="logout.php">LOGOUT</a></li>
      </ul>
  </div>
  </div>
</nav>
</body>
</html>